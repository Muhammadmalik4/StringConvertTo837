﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StringConvertTo837.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private List<string> SegmentsList = new List<string>();
        private List<string> ElementsList = new List<string>();
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ParseClaim()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [HttpPost]
        public async Task<List<string>> StringTo837(string mainString)
        {
            try
            {
                if (mainString.Contains("ISA") && mainString.Contains("IEA"))
                {
                    string[] arrString = mainString.Split('~');
                    SegmentsList.Add("Total segments in this string = " + arrString.Length.ToString());
                    //SegmentsList.Add("Involved segments are = ");
                    //for (int i = 0; i < arrString.Length; i++)
                    //{
                    //    SegmentsList.Add(arrString[i].ToString());
                    //}
                    // SegmentsList.Add("Elements Details=");
                    for (int i = 0; i < arrString.Length; i++)
                    {
                        string segments = arrString[i];
                        //string[] elements = segments.Split('*');
                        //SegmentsList.Add("Total Element in " + elements[0] + " = " + elements.Length);
                        string details = await ValidateSegments(segments);
                        if (details != "")
                        {

                            ElementsList.Add(details);
                        }
                    }
                    SegmentsList.Add("String Parsed Successfully.");
                    string[] results = ElementsList.ToArray<string>();
                    for (int x = 0; x < results.Length; x++)
                    {
                        // string y = results[x].Replace(" ", "");
                        SegmentsList.Add("" + results[x]);
                    }
                    return SegmentsList;
                }
                else
                {
                    throw new ArgumentException("Parameter cannot be null");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<string> ValidateSegments(string segment)
        {
            try
            {
                ValidatorController _validator = new ValidatorController();
                string segmentDetails = "";
                string[] elements = new string[30];
                string[] arr = segment.Split('*');
                for (int i = 0; i < elements.Length; i++)
                {
                    if (i < arr.Length)
                    {
                        elements[i] = arr[i];
                    }
                    else
                    {
                        elements[i] = "";
                    }
                }
                if (elements[0] != "ISA" && elements[0] != "GE" && elements[0]!= "IEA" && elements[0] != " " && elements[0] != "GS")
                {
                    segmentDetails = elements[0] + "=>";
                }
                if (elements[0] == "LIN")
                {

                    segmentDetails +=await _validator.LIN_Methode(elements);
                }
                if (elements[0] == "CRC")
                {
                    segmentDetails += await _validator.CRC_Methode(elements);
                }
                if (elements[0] == "CTP")
                {
                    segmentDetails += await _validator.CTP_Methode(elements);
                }
                if (elements[0] == "SV1")
                {
                    segmentDetails += await _validator.SV1_Methode(elements);
                }
                if (elements[0] == "SV5")
                {
                    segmentDetails += await _validator.SV5_Methode(elements);
                }
                if (elements[0] == "PS1")
                {
                    segmentDetails += await _validator.PS1_Methode(elements);
                }
                if (elements[0] == "HCP")
                {
                    segmentDetails += await _validator.HCP_Methode(elements);
                }
                if (elements[0] == "ST")
                {
                    segmentDetails += await _validator.ST_Methode(elements);
                }
                if (elements[0] == "NM1")
                {
                    segmentDetails += await _validator.NM1_Methode(elements);
                }
                if (elements[0] == "BHT")
                {
                    segmentDetails += await _validator.BHT_Methode(elements);
                }
                if (elements[0] == "CLM")
                {
                    segmentDetails += await _validator.CLM_Methode(elements);
                }
                if (elements[0] == "SBR")
                {
                    segmentDetails += await _validator.SBR_Methode(elements);
                }
                if (elements[0] == "CUR")
                {
                    segmentDetails += await _validator.CUR_Methode(elements);
                }
                if (elements[0] == "CAS")
                {
                    segmentDetails += await _validator.CAS_Methode(elements);
                }
                if (elements[0] == "N3")
                {
                    segmentDetails += await _validator.N3_Methode(elements);
                }
                if (elements[0] == "N4")
                {
                    segmentDetails += await _validator.N4_Methode(elements);
                }
                if (elements[0] == "PER")
                {
                    segmentDetails += await _validator.PER_Methode(elements);
                }
                if (elements[0] == "DTP")
                {
                    segmentDetails += await _validator.DTP_Methode(elements);
                }
                if (elements[0] == "HL")
                {
                    segmentDetails += await _validator.HL_Methode(elements);

                }
                if (elements[0] == "REF")
                {
                    segmentDetails += await _validator.REF_Methode(elements);
                }
                if (elements[0] == "PRV")
                {
                    segmentDetails += await _validator.PRV_Methode(elements);
                }
                if (elements[0] == "AMT")
                {
                    segmentDetails += await _validator.AMT_Methode(elements);

                }
                if (elements[0] == "SE")
                {
                    segmentDetails += await _validator.SE_Methode(elements);
                }
                if (elements[0] == "LX")
                {
                    segmentDetails += await _validator.LX_Methode(elements);

                }
                if (elements[0] == "PAT")
                {
                    segmentDetails += await _validator.PAT_Methode(elements);
                }
                if (elements[0] == "CR3")
                {
                    segmentDetails += await _validator.CR3_Methode(elements);
                }
                if (elements[0] == "QTY")
                {
                    segmentDetails += await _validator.QTY_Methode(elements);
                }
                if (elements[0] == "LQ")
                {
                    segmentDetails += await _validator.LQ_Methode(elements);
                }
                if (elements[0] == "FRM")
                {
                    segmentDetails += await _validator.FRM_Methode(elements);
                }
                if (elements[0] == "K3")
                {
                    segmentDetails += await _validator.K3_Methode(elements);
                }
                if (elements[0] == "NTE")
                {
                    segmentDetails += await _validator.NTE_Methode(elements);
                }
                if (elements[0] == "DMG")
                {
                    segmentDetails += await _validator.DMG_Methode(elements);
                }
                if (elements[0] == "CN1")
                {
                    segmentDetails += await _validator.CN1_Methode(elements);
                }
                if (elements[0] == "HI")
                {
                    segmentDetails += await _validator.HI_Methode(elements);
                }
                if (elements[0] == "CR1")
                {
                    segmentDetails += await _validator.CR1_Methode(elements);
                }
                if (elements[0] == "SVD")
                {
                    segmentDetails += await _validator.SVD_Methode(elements);
                }
                if (elements[0] == "CR2")
                {
                    segmentDetails += await _validator.CR2_Methode(elements);
                }
                if (elements[0] == "PWK")
                {
                    segmentDetails += await _validator.PWK_Methode(elements);
                }
                return segmentDetails;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}