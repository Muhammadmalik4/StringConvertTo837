﻿using Microsoft.AspNetCore.Mvc;
using StringConvertTo837.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Controllers
{
    public class ValidatorController : Controller
    {
        private static List<AMT> AMT_Segment = new List<AMT>();
        private static List<BHT> BHT_Segment = new List<BHT>();
        private static List<CAS> CAS_Segment = new List<CAS>();
        private static List<CLM> CLM_Segment = new List<CLM>();
        private static List<CN1> CN1_Segment = new List<CN1>();
        private static List<CUR> CUR_Segment = new List<CUR>();
        private static List<DMG> DMG_Segment = new List<DMG>();
        private static List<DTP> DTP_Segment = new List<DTP>();
        private static List<HL> HL_Segment = new List<HL>();
        private static List<LX> LX_Segment = new List<LX>();
        private static List<N3> N3_Segment = new List<N3>();
        private static List<N4> N4_Segment = new List<N4>();
        private static List<NM1> NM1_Segment = new List<NM1>();
        private static List<PAT> PAT_Segment = new List<PAT>();
        private static List<PER> PER_Segment = new List<PER>();
        private static List<PRV> PRV_Segment = new List<PRV>();
        private static List<PWK> PWK_Segment = new List<PWK>();
        private static List<REF> REF_Segment = new List<REF>();
        private static List<SBR> SBR_Segment = new List<SBR>();
        private static List<SE> SE_Segment = new List<SE>();
        private static List<ST> ST_Segment = new List<ST>();
        private static List<CR1> CR1_Segment = new List<CR1>();
        private static List<HI> HI_Segment = new List<HI>();
        private static List<NTE> NTE_Segment = new List<NTE>();
        private static List<K3> K3_Segment = new List<K3>();
        private static List<FRM> FRM_Segment = new List<FRM>();
        private static List<LQ> LQ_Segment = new List<LQ>();
        private static List<QTY> QTY_Segment = new List<QTY>();
        private static List<CR3> CR3_Segment = new List<CR3>();
        private static List<PS1> PS1_Segment = new List<PS1>();
        private static List<SVD> SVD_Segment = new List<SVD>();
        private static List<CRC> CRC_Segment = new List<CRC>();
        private static List<HCP> HCP_Segment = new List<HCP>();
        private static List<SV5> SV5_Segment = new List<SV5>();
        private static List<CTP> CTP_Segment = new List<CTP>();
        private static List<SV1> SV1_Segment = new List<SV1>();
        private static List<LIN> LIN_Segment = new List<LIN>();
        public async Task<string> ST_Methode(string[] ST_Array)
        {
            ST obj = new ST();
            string STDetails = "";
            if (ST_Array[1].Length == 3)
            {
                STDetails += "ST<>837";
                obj.IdCode = 837;
            }
            if (ST_Array[2].Length >= 3 && ST_Array[2].Length <= 9)
            {
                obj.ControlNumber = ST_Array[2];
                STDetails += "<>Transaction Set Control Number is =" + ST_Array[2];
            }
            if (ST_Array[3].Length > 0 && ST_Array[3].Length < 36)
            {
                obj.ImpleConvReference = ST_Array[3];
                STDetails += "<>Implementation Convention Reference is =" + ST_Array[3];
            }
            ST_Segment.Add(obj);
            return STDetails;
        }
        public async Task<string> BHT_Methode(string[] elements)
        {
            BHT obj = new BHT();
            string segmentDetails = "";
            if (elements[1] == "0019")
            {
                obj.HierarchStructCode = elements[1];
                segmentDetails += "Information Source, Subscriber,Dependent_Hierarch Struct Code=" + elements[1].ToString();
            }
            if (elements[2] == "00")
            {
                obj.TSPurposeCode = elements[2];
                segmentDetails += "<>Original TS Purpose Code= " + elements[2].ToString();
            }
            else if (elements[2] == "18")
            {
                obj.TSPurposeCode = elements[2];
                segmentDetails += "<>Reissue TS Purpose Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ReferenceIdent = elements[3];
                segmentDetails += "<>Reference Ident= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.Date = elements[4].ToString();
                segmentDetails += "<>Date=" + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.Time = elements[5];
                segmentDetails += "<>Time=" + elements[5].ToString();
            }
            if (elements[6] == "31")
            {
                obj.TransactionTypeCode = elements[6];
                segmentDetails += "<>Subrogation Demand,Transaction Type Code=" + elements[6].ToString();
            }
            else if (elements[6] == "CH")
            {
                obj.TransactionTypeCode = elements[6];
                segmentDetails += "<>Chargeable,Transaction Type Code=" + elements[6].ToString();
            }
            else if (elements[6] == "RP")
            {
                obj.TransactionTypeCode = elements[6];
                segmentDetails += "<>Reporting,Transaction Type Code=" + elements[6].ToString();
            }
            BHT_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> NM1_Methode(string[] elements)
        {
            NM1 obj = new NM1();
            string segmentDetails = "";
            if (elements[1] == "40")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Receiver<>";
            }
            else if (elements[1] == "IL")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Subscribe<>";
            }
            else if (elements[1] == "PR")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Payer<>";
            }
            else if (elements[1] == "QC")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Patient<>";
            }
            else if (elements[1] == "DN")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Referring Provider<>";
            }
            else if (elements[1] == "77")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Service Location<>";
            }
            else if (elements[1] == "41")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Submitter<>";
            }
            else if (elements[1] == "85")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Billing Provider<>";
            }
            else if (elements[1] == "87")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Pay-to Provider<>";
            }
            else if (elements[1] == "PE")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Payee<>";
            }
            else if (elements[1] == "82")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Rendering Provider<>";
            }
            else if (elements[1] == "77")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Service Location<>";
            }
            else if (elements[1] == "DQ")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Supervising Physician<>";
            }
            else if (elements[1] == "PW")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Pickup Address<>";
            }
            else if (elements[1] == "45")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Drop-off Location<>";
            }
            else if (elements[1] == "QB")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Purchase Service Provider<>";
            }
            else if (elements[1] == "DK")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Ordering Physician<>";
            }
            if (elements[2] == "2")
            {
                obj.EntityTypeQualifier = elements[2];
                segmentDetails += "Non-Person Entity";
            }
            if (elements[2] == "1")
            {
                obj.EntityTypeQualifier = elements[2];
                segmentDetails += "Person";
            }
            if (elements[3] != "" && elements[3] != null)
            {
                obj.NameLastOrgName = elements[3];
                segmentDetails += "<>Name =" + elements[3].ToString();
            }
            if (elements[4] != "" && elements[4] != null)
            {
                obj.NameFirst = elements[4];
                segmentDetails += "<>First Name =" + elements[4];
            }
            if (elements[5] != "" && elements[5] != null)
            {
                obj.NameMiddle = elements[5];
                segmentDetails += "<>Middle Name =" + elements[5];
            }
            if (elements[6] != "" && elements[6] != null)
            {
                obj.NamePrefix = elements[6];
                segmentDetails += "<>Prefix Name =" + elements[6];
            }
            if (elements[7] != "" && elements[7] != null)
            {
                obj.NameSuffix = elements[7];
                segmentDetails += "<>Suffix Name =" + elements[7];
            }
            if (elements[8].Length > 0 && elements[8].Length < 3)
            {
                obj.IDCodeQualifier = elements[8];
                segmentDetails += "<>Id Code Qualifier =" + elements[8];
            }
            if (elements[9].Length > 1 && elements[9].Length < 81)
            {
                obj.IDCode = elements[9];
                segmentDetails += "<>Id Code =" + elements[9];
            }
            if (elements[10].Length > 0 && elements[10].Length < 3)
            {
                obj.EntityRelatCode = elements[10];
                segmentDetails += "<>Entity Relat Code =" + elements[10];
            }
            if (elements[11].Length > 1 && elements[11].Length < 81)
            {
                obj.EntityIDCodeReapet = elements[11];
                segmentDetails += "<>Entity Id Code =" + elements[11];
            }
            if (elements[12].Length > 1 && elements[12].Length < 81)
            {
                obj.NameLastOrgNameReapet = elements[12];
                segmentDetails += "<>Name =" + elements[12];
            }
            NM1_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CLM_Methode(string[] elements)
        {
            CLM obj = new CLM();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.ClaimSubmtIdentifier = elements[1];
                segmentDetails += " Claim Submt Identifier= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2];
                segmentDetails += "<>Monetary Amount= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ClaimFileIndCode = elements[3];
                segmentDetails += "<>Claim File Ind Code= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.NonInstClaimCode = elements[4];
                segmentDetails += "<>Non-Inst Claim Code=" + elements[4].ToString();
            }
            if (elements[5] == "1331")
            {
                obj.HealthCareServLoc = elements[5];

                segmentDetails += "<>Facility Code Value=" + elements[5].ToString();
            }
            else if (elements[5] == "1332")
            {
                obj.HealthCareServLoc = elements[5];
                segmentDetails += "<>Facility Code Qualifier=" + elements[5].ToString();
            }
            else
            {
                obj.HealthCareServLoc = elements[5];
                segmentDetails += "<>Claim Frequency Type Code=" + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.YesNoCondRespCode = elements[6];
                segmentDetails += "<>Yes/No Cond Resp Cond=" + elements[6].ToString();
            }
            if (elements[7] == "A")
            {
                obj.ProviderAcceptCode = elements[7];
                segmentDetails += "<>Assigned=" + elements[7].ToString();
            }
            else if (elements[7] == "B")
            {
                obj.ProviderAcceptCode = elements[7];
                segmentDetails += "<>Assignment Accepted on Clinical Lab Services Only=" + elements[7].ToString();
            }
            else
            {
                obj.ProviderAcceptCode = elements[7];
                segmentDetails += "<>Not Assigned=" + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.YesNoCondRespCodeR1 = elements[8];
                segmentDetails += "<>Yes/No Cond Resp Cond=" + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.ReleaseofInfoCode = elements[9];
                segmentDetails += "<>Release of Info Code=" + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.PatientSigSourceCode = elements[10];
                segmentDetails += "<>Patient Sig Source Code=" + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.RelatedCausesInfo = elements[11];
                segmentDetails += "<>Related Causes Info=" + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                obj.SpecialProgCode = elements[12];
                segmentDetails += "<>Special Prog Code=" + elements[12].ToString();
            }
            if (elements[13] != "")
            {
                obj.YesNoCondRespCodeR2 = elements[13];
                segmentDetails += "<>Yes/No Cond Resp Cond=" + elements[13].ToString();
            }
            if (elements[14] != "")
            {
                obj.LevelofServCode = elements[14];
                segmentDetails += "<>Level of Serv Code=" + elements[14].ToString();
            }
            if (elements[15] != "")
            {
                obj.YesNoCondRespCodeR3 = elements[15];
                segmentDetails += "<>Yes/No Cond Resp Cond=" + elements[15].ToString();
            }
            if (elements[16] != "")
            {
                obj.ProviderAgreeCode = elements[16];
                segmentDetails += "<>Provider Agree Code=" + elements[16].ToString();
            }
            if (elements[17] != "")
            {
                obj.ClaimStatusCode = elements[17];
                segmentDetails += "<>Claim Status Code=" + elements[17].ToString();
            }
            if (elements[18] != "")
            {
                obj.YesNoCondRespCodeR4 = elements[18];
                segmentDetails += "<>Yes/No Cond Resp Cond=" + elements[18].ToString();
            }
            if (elements[19] != "")
            {
                obj.ClaimSubmtReasonCode = elements[19];
                segmentDetails += "<>Claim Submt Reason Code=" + elements[19].ToString();
            }
            if (elements[20] != "")
            {
                obj.DelayReasonCode = elements[20];
                segmentDetails += "<>Delay Reason Code=" + elements[20].ToString();
            }
            CLM_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> SBR_Methode(string[] elements)
        {
            SBR obj = new SBR();
            string segmentDetails = "";
            if (elements[1] == "A")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Payer Responsibility Four=" + elements[1].ToString();
            }
            else if (elements[1] == "B")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Payer Responsibility Five=" + elements[1].ToString();
            }
            else if (elements[1] == "C")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += " Payer Responsibility Six=" + elements[1].ToString();
            }
            else if (elements[1] == "D")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Payer Responsibility Seven=" + elements[1].ToString();
            }
            else if (elements[1] == "E")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += " Payer Responsibility Eight=" + elements[1].ToString();
            }
            else if (elements[1] == "F")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Payer Responsibility Nine=" + elements[1].ToString();
            }
            else if (elements[1] == "G")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += " Payer Responsibility Ten=" + elements[1].ToString();
            }
            else if (elements[1] == "H")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += " Payer Responsibility Eleven=" + elements[1].ToString();
            }
            else if (elements[1] == "P")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += " Primary=" + elements[1].ToString();
            }
            else if (elements[1] == "S")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Secondary=" + elements[1].ToString();
            }
            else if (elements[1] == "T")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Tertiary=" + elements[1].ToString();
            }
            else if (elements[1] == "U")
            {
                obj.PayerRespSeqNoCode = elements[1];
                segmentDetails += "Unknown=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.IndividualRelatCode = elements[2];
                segmentDetails += "<>Individual Relat Code = " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ReferenceIdent = elements[3];
                segmentDetails += "<>Reference Ident= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.Name = elements[4];
                segmentDetails += "<>Name= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.InsuranceTypeCode = elements[5];
                segmentDetails += "<>Insurance Type Code=" + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.BenefitsCoordCode = elements[6];
                segmentDetails += "<>Benefits Coord Code=" + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.YesNoCondRespCode = elements[7];
                segmentDetails += "<>Yes/No Cond Resp Cond=" + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.EmploymentStatusCode = elements[8];
                segmentDetails += "<>Employment Status Code=" + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.ClaimFileIndCode = elements[9];
                segmentDetails += "<>Claim File Ind Code = " + elements[9].ToString();
            }
            SBR_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CUR_Methode(string[] elements)
        {
            CUR obj = new CUR();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.EntityIDCode = elements[1];
                segmentDetails += "Entity Id Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.CurrencyCode = elements[2];
                segmentDetails += "<>Currency Code = " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ExchangeRate = elements[3];
                segmentDetails += "<>Exchange Rate= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.EntityIDCode = elements[4];
                segmentDetails += "<>Entity Id Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.CurrencyCodeR1 = elements[5];
                segmentDetails += "<>Currency Code = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.CurrMarketExchgCode = elements[6];
                segmentDetails += "<>Curr Market/Exchg Code = " + elements[6].ToString();
            }
            for (int i = 1; i < 6; i++)
            {
                if (elements[6 + i] != "")
                {
                    segmentDetails += "<>Date/Time Qualifier = " + elements[6 + i].ToString();
                }
                if (elements[7 + i] != "")
                {
                    segmentDetails += "<>Date = " + elements[7 + i].ToString();
                }
                if (elements[8 + i] != "")
                {
                    segmentDetails += "<>Time = " + elements[8 + i].ToString();
                }
            }
            CUR_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> N3_Methode(string[] elements)
        {
            N3 obj = new N3();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.AddressInformation = elements[1];
                segmentDetails += "Address Information=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.AddressInformationR1 = elements[2];
                segmentDetails += "<>Address Information=" + elements[2];
            }
            N3_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> N4_Methode(string[] elements)
        {
            N4 obj = new N4();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.CityName = elements[1];
                segmentDetails += " CityName=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.StateorProvCode = elements[2];
                segmentDetails += "<>State or Prov Code = " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.PostalCode = elements[3];
                segmentDetails += "<>Postal Code = " + elements[3].ToString();
            }

            if (elements[4] != "")
            {
                obj.CountryCode = elements[4];
                segmentDetails += "<>Country Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.LocationQualifier = elements[5];
                segmentDetails += "<>Location Qualifier= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.LocationIdentifier = elements[6];

                segmentDetails += "<>Location Identifier = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.CountrySubCode = elements[7];
                segmentDetails += "<>Country Sub Code = " + elements[7].ToString();
            }
            N4_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> DTP_Methode(string[] elements)
        {
            DTP obj = new DTP();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.DateTimeQualifier = elements[1];
                segmentDetails += " Date/Time Qualifier=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.DateTimeFormatQua = elements[2];
                segmentDetails += "<>Date Time Format Qual = " + elements[2].ToString() + "(CCYYMMDD)";
            }

            if (elements[3] != "")
            {
                obj.DateTimePeriod = elements[3];
                segmentDetails += "<>Date Time Period = " + elements[3].ToString();
            }
            DTP_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> HL_Methode(string[] elements)
        {
            HL obj = new HL();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.HierarchIDNumber = elements[1];
                segmentDetails += " Hierarch ID Number=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.HierarchParentID = elements[2];
                segmentDetails += "<>Hierarch Parent ID= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.HierarchParentID = elements[3];
                segmentDetails += "<>Hierarch Level Code = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.HierarchChildCode = elements[4];
                segmentDetails += "<>Hierarch Child Code = " + elements[4].ToString();
            }
            HL_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> REF_Methode(string[] elements)
        {
            REF obj = new REF();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.ReferenceIdentQual = elements[1];
                segmentDetails += " Reference Ident Qual=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.ReferenceIdent = elements[2];
                segmentDetails += "<>Reference Ident= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.Description = elements[3];
                segmentDetails += "<>Description= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.ReferenceIdentifier = elements[4];
                segmentDetails += "<>Reference Identifier = " + elements[4].ToString();
            }
            REF_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> PRV_Methode(string[] elements)
        {
            PRV obj = new PRV();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.ProviderCode = elements[1];
                segmentDetails += " Provider Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.ReferenceIdentQual = elements[2];
                segmentDetails += "<>Reference Ident Qual= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ReferenceIdent = elements[3];
                segmentDetails += "<>Reference Ident= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.StateorProvCode = elements[4];
                segmentDetails += "<>State or Prov Code= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.ProviderSpecInf = elements[5];
                segmentDetails += "<>Provider Spec Inf= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.ProviderOrgCode = elements[6];
                segmentDetails += "<>Provider Org Code= " + elements[6].ToString();
            }
            PRV_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> AMT_Methode(string[] elements)
        {
            AMT obj = new AMT();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.AmountQualCode = elements[1];
                segmentDetails += " Amount Qual Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2];
                segmentDetails += "<>Monetary Amount= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.CredDebitFlagCode = elements[3];
                segmentDetails += "<>Cred/Debit Flag Code = " + elements[3].ToString();
            }
            AMT_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> PAT_Methode(string[] elements)
        {
            PAT obj = new PAT();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.IndividualRelatCode = elements[1];
                segmentDetails += " Individual Relat Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.PatientLocCode = elements[2];
                segmentDetails += "<>Patient Loc Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.EmploymentStatuSCode = elements[3];
                segmentDetails += "<>Employment Status Code=" + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.StudentStatusCode = elements[4];
                segmentDetails += "<>Student Status Code= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.DateTimeFormatQual = elements[5];
                segmentDetails += "<>Date Time Format Qual=" + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.DateTimePeriod = elements[6];
                segmentDetails += "<>Date Time Period= " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.UnitBasisMeasCode = elements[7];
                segmentDetails += "<>Unit/Basis Meas Code = " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.Weight = elements[8];
                segmentDetails += "<>Weight= " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.YesNoCondRespCode = elements[9];
                segmentDetails += " Yes/No Cond Resp Code = " + elements[9].ToString();
            }
            PAT_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> PER_Methode(string[] elements)
        {
            PER obj = new PER();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.ContactFunctCode = elements[1];
                segmentDetails += "Contact Funct Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.Name = elements[2];
                segmentDetails += "<>Name = " + elements[2].ToString();
            }

            if (elements[3] == "EM")
            {
                obj.CommNumberQual = elements[3];
                segmentDetails += "<>Electronic Mail = " + elements[3].ToString();
            }
            if (elements[3] == "FX")
            {
                obj.CommNumberQual = elements[3];
                segmentDetails += "<>Facsimile = " + elements[3].ToString();
            }
            if (elements[3] == "TE")
            {
                obj.CommNumberQual = elements[3];
                segmentDetails += "<>Telephone = " + elements[3].ToString();
            }
            if (elements[3] == "EX")
            {
                obj.CommNumberQual = elements[3];
                segmentDetails += "<>Telephone Extension = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.CommNumber = elements[4];
                segmentDetails += "<>Comm Number = " + elements[4].ToString();
            }

            if (elements[5] == "EM")
            {
                obj.CommNumberQualR1 = elements[5];
                segmentDetails += "<>Electronic Mail = " + elements[5].ToString();
            }
            if (elements[5] == "FX")
            {
                obj.CommNumberQualR1 = elements[5];
                segmentDetails += "<>Facsimile = " + elements[5].ToString();
            }
            if (elements[5] == "TE")
            {
                obj.CommNumberQualR1 = elements[5];
                segmentDetails += "<>Telephone = " + elements[5].ToString();
            }
            if (elements[5] == "EX")
            {
                obj.CommNumberQualR1 = elements[5];
                segmentDetails += "<>Telephone Extension = " + elements[5].ToString();
            }

            if (elements[6] != "")
            {
                obj.CommNumberR1 = elements[6];
                segmentDetails += "<>Comm Number = " + elements[6].ToString();
            }
            if (elements[7] == "EM")
            {
                obj.CommNumberQualR2 = elements[7];
                segmentDetails += "<>Electronic Mail = " + elements[7].ToString();
            }
            if (elements[7] == "FX")
            {
                obj.CommNumberQualR2 = elements[7];
                segmentDetails += "<>Facsimile = " + elements[7].ToString();
            }
            if (elements[7] == "TE")
            {
                obj.CommNumberQualR2 = elements[7];
                segmentDetails += "<>Telephone = " + elements[7].ToString();
            }
            if (elements[7] == "EX")
            {
                obj.CommNumberQualR2 = elements[7];
                segmentDetails += "<>Telephone Extension = " + elements[7].ToString();
            }

            if (elements[8] != "")
            {
                obj.CommNumberR2 = elements[8];
                segmentDetails += "<>Comm Number = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.ContactInqReference = elements[9];
                segmentDetails += "<>Contact Inq Reference = " + elements[9].ToString();
            }
            PER_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> DMG_Methode(string[] elements)
        {
            DMG obj = new DMG();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.DateTimeFormatQual = elements[1];
                segmentDetails += " Date Time Format Qual = " + elements[1].ToString() + "(CCYYMMDD)";
            }
            if (elements[2] != "")
            {
                obj.DateTimePeriod = elements[2];
                segmentDetails += "<>Date Time Period = " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.GenderCode = elements[3];
                segmentDetails += "<>Gender Code = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.MaritalStatusCode = elements[4];
                segmentDetails += "<>Marital Status Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.CompRaceorEthnInf = elements[5];
                segmentDetails += "<>Comp Race or Ethn Inf = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.CitizenshipStatusCode = elements[6];
                segmentDetails += "<>Citizenship Status Code = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.CountryCode = elements[7];
                segmentDetails += "<>Country Code = " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.BasisofVerifCode = elements[8];
                segmentDetails += "<>Basis of Verif Code = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.Quantity = elements[9];
                segmentDetails += "<>Quantity = " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.CodeListQualCode = elements[10];
                segmentDetails += "<>Code List Qual Code = " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.IndustryCode = elements[11];
                segmentDetails += "<>Industry Code = " + elements[11].ToString();
            }
            DMG_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CN1_Methode(string[] elements)
        {
            CN1 obj = new CN1();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.ContractTypeCode = elements[1];
                segmentDetails += " Contract Type Code = " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2];
                segmentDetails += "<>Monetary Amount = " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.AllowChrgPercent = elements[3];
                segmentDetails += "<>Allow/Chrg Percent = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.ReferenceIdent = elements[4];
                segmentDetails += "<>Reference Ident = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.TermsDiscPercent = elements[5];
                segmentDetails += "<>Terms Disc Percent = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.VersionID = elements[6];
                segmentDetails += "<>Version ID = " + elements[6].ToString();
            }
            CN1_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> PWK_Methode(string[] elements)
        {
            PWK obj = new PWK();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.ReportTypeCode = elements[1];
                segmentDetails += " Report Type Code = " + elements[1].ToString() + "(CCYYMMDD)";
            }
            if (elements[2] != "")
            {
                obj.ReportTransmCode = elements[2];
                segmentDetails += "<>Report Transm Code = " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ReportCopiesNeed = elements[3];
                segmentDetails += "<>Report Copies Need = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.EntityIDCode = elements[4];
                segmentDetails += "<>Entity ID Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.IDCodeQualifier = elements[5];
                segmentDetails += "<>ID Code Qualifier = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.IDCode = elements[6];
                segmentDetails += "<>ID Code = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.Description = elements[7];
                segmentDetails += "<>Description = " + elements[7].ToString();
            }

            if (elements[8] != "")
            {
                obj.ActionsIndicated = elements[8];
                segmentDetails += "<>Actions Indicated = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.RequestCategCode = elements[9];
                segmentDetails += "<>Request Categ Code= " + elements[9].ToString();
            }
            PWK_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CAS_Methode(string[] elements)
        {
            CAS OBJ = new CAS();
            string segmentDetails = "";
            if (elements[1] == "CO")
            {
                OBJ.ClaimAdjGroupCode = elements[1];
                segmentDetails += " Contractual Obligations= " + elements[1].ToString();
            }
            else if (elements[1] == "CR")
            {
                OBJ.ClaimAdjGroupCode = elements[1];
                segmentDetails += " Correction and Reversals= " + elements[1].ToString();
            }
            else if (elements[1] == "OA")
            {
                OBJ.ClaimAdjGroupCode = elements[1];
                segmentDetails += " Other adjustments= " + elements[1].ToString();
            }
            else if (elements[1] == "PI")
            {
                OBJ.ClaimAdjGroupCode = elements[1];
                segmentDetails += "  Payor Initiated Reductions= " + elements[1].ToString();
            }
            else if (elements[1] == "PR")
            {
                OBJ.ClaimAdjGroupCode = elements[1];
                segmentDetails += " Patient Responsibility= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                OBJ.ClaimAdjReasonCode = elements[2];
                segmentDetails += "<>Claim Adj Reason Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                OBJ.MonetaryAmount = elements[3];
                segmentDetails += "<>Monetary Amount= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                OBJ.Quantity = elements[4];
                segmentDetails += "<>Quantity=" + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                OBJ.ClaimAdjReasonCodeR1 = elements[5];
                segmentDetails += "<>Claim Adj Reason Code=" + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                OBJ.MonetaryAmountR1 = elements[6];

                segmentDetails += "<>Monetary Amount=" + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                OBJ.QuantityR1 = elements[7];
                segmentDetails += "<>Quantity=" + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                OBJ.ClaimAdjReasonCodeR2 = elements[8];
                segmentDetails += "<>Claim Adj Reason Code=" + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                OBJ.MonetaryAmountR2 = elements[9];
                segmentDetails += "<>Monetary Amount=" + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                OBJ.QuantityR2 = elements[10];
                segmentDetails += "<>Quantity=" + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                OBJ.ClaimAdjReasonCodeR3 = elements[11];
                segmentDetails += "<>Claim Adj Reason Code=" + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                OBJ.MonetaryAmountR3 = elements[12];
                segmentDetails += "<>Monetary Amount=" + elements[12].ToString();
            }
            if (elements[13] != "")
            {
                OBJ.QuantityR3 = elements[13];
                segmentDetails += "<>Quantity=" + elements[13].ToString();
            }
            if (elements[14] != "")
            {
                OBJ.ClaimAdjReasonCodeR4 = elements[14];
                segmentDetails += "<>Claim Adj Reason Code=" + elements[14].ToString();
            }
            if (elements[15] != "")
            {
                OBJ.MonetaryAmountR4 = elements[15];
                segmentDetails += "<>Monetary Amount=" + elements[15].ToString();
            }
            if (elements[16] != "")
            {
                OBJ.QuantityR4 = elements[16];
                segmentDetails += "<>Quantity=" + elements[16].ToString();
            }
            if (elements[17] != "")
            {
                OBJ.ClaimAdjReasonCodeR5 = elements[17];
                segmentDetails += "<>Claim Adj Reason Code=" + elements[17].ToString();
            }
            if (elements[18] != "")
            {
                OBJ.MonetaryAmountR5 = elements[18];
                segmentDetails += "<>Monetary Amount=" + elements[18].ToString();
            }
            if (elements[19] != "")
            {
                OBJ.QuantityR5 = elements[19];
                segmentDetails += "<>Quantity=" + elements[19].ToString();
            }
            CAS_Segment.Add(OBJ);
            return segmentDetails;
        }
        public async Task<string> LX_Methode(string[] elements)
        {
            LX obj = new LX();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.AssignedNumber = elements[1];
                segmentDetails += " Assigned Number=" + elements[1].ToString();
            }
            LX_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> SE_Methode(string[] elements)
        {
            SE obj = new SE();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.NumberofIncSegs = elements[1];
                segmentDetails += " Number of Inc Segs=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.ControlNumber = elements[2];
                segmentDetails += "<>TS Control Number= " + elements[2].ToString();
            }
            SE_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CR3_Methode(string[] elements)
        {
            CR3 obj = new CR3();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.CertificateTypeCode = elements[1].ToString();
                segmentDetails += " Certificate Type Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.UnitBasisMeasCode = elements[2].ToString();
                segmentDetails += "<>Unit/Basis Meas Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.Quantity = elements[3].ToString();
                segmentDetails += "<>Quantity=" + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.InsulinDependCode = elements[4].ToString();
                segmentDetails += "<>Insulin Depend Code= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.Description = elements[5].ToString();
                segmentDetails += "<>Description=" + elements[5].ToString();
            }

            CR3_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> QTY_Methode(string[] elements)
        {
            QTY obj = new QTY();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.QuantityQualifier = elements[1].ToString();
                segmentDetails += " Quantity Qualifier=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.Quantity = elements[2].ToString();
                segmentDetails += "<>Quantity= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.CompositeUnitofMea = elements[3].ToString();
                segmentDetails += "<>Composite Unit of Mea=" + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.FreeFormMessage = elements[4].ToString();
                segmentDetails += "<>Free Form Message= " + elements[4].ToString();
            }
            QTY_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> LQ_Methode(string[] elements)
        {
            LQ obj = new LQ();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.CodeListQualCode = elements[1].ToString();
                segmentDetails += " Code List Qual Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.IndustryCode = elements[2].ToString();
                segmentDetails += "<>Industry Code= " + elements[2].ToString();
            }
            LQ_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> FRM_Methode(string[] elements)
        {
            FRM obj = new FRM();

            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.AssignedID = elements[1].ToString();
                segmentDetails += " Assigned ID=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.YesNoCondRespCode = elements[2].ToString();
                segmentDetails += "<>Yes/No Cond Resp Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ReferenceIdent = elements[3].ToString();
                segmentDetails += " Reference Ident=" + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.Date = elements[4].ToString();
                segmentDetails += "<>Date= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.AllowChrgPercent = elements[5].ToString();
                segmentDetails += " Allow/Chrg Percent=" + elements[5].ToString();
            }
            FRM_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> K3_Methode(string[] elements)
        {
            K3 obj = new K3();

            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.FixedFormInformation = elements[1].ToString();
                segmentDetails += " Fixed Form Information=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.RecordFormatCode = elements[2].ToString();
                segmentDetails += "<>Record Format Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.CompositeUnitofMea = elements[3].ToString();
                segmentDetails += " Composite Unit of Mea=" + elements[3].ToString();
            }
            K3_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> NTE_Methode(string[] elements)
        {
            NTE obj = new NTE();

            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.NoteRefCode = elements[1].ToString();
                segmentDetails += " Note Ref Code=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.Description = elements[2].ToString();
                segmentDetails += "<>Description= " + elements[2].ToString();
            }
            NTE_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> HI_Methode(string[] elements)
        {
            HI obj = new HI();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.HealthCareCodeInfo = elements[1].ToString();
                segmentDetails += " Health Care Code Info = " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.HealthCareCodeInfoR1 = elements[2].ToString();
                segmentDetails += "<>Health Care Code Info= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.HealthCareCodeInfoR2 = elements[3].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.HealthCareCodeInfoR3 = elements[4].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.HealthCareCodeInfoR4 = elements[5].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.HealthCareCodeInfoR5 = elements[6].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.HealthCareCodeInfoR6 = elements[7].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.HealthCareCodeInfoR7 = elements[8].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.HealthCareCodeInfoR8 = elements[9].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.HealthCareCodeInfoR9 = elements[10].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.HealthCareCodeInfoR10 = elements[11].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                obj.HealthCareCodeInfoR11 = elements[12].ToString();
                segmentDetails += "<>Health Care Code Info = " + elements[12].ToString();
            }
            HI_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CR1_Methode(string[] elements)
        {
            CR1 obj = new CR1();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.UnitBasisMeasCode = elements[1].ToString();
                segmentDetails += " Unit/Basis Meas Code = " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.Weight = elements[2].ToString();
                segmentDetails += "<>Weight= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.AmbulanceTransCode = elements[3].ToString();
                segmentDetails += "<>Ambulance Trans Code= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.AmbulanceReasonCode = elements[4].ToString();
                segmentDetails += "<>Ambulance Reason Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.UnitBasisMeasCodeR1 = elements[5].ToString();
                segmentDetails += "<>Unit/Basis Meas Code = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.Quantity = elements[6].ToString();
                segmentDetails += "<>Quantity = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.AddressInformation = elements[7].ToString();
                segmentDetails += "<>Address Information = " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.AddressInformationR1 = elements[8].ToString();
                segmentDetails += "<>Address Information  = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.Description = elements[9].ToString();
                segmentDetails += "<>Description = " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.DescriptionR1 = elements[10].ToString();
                segmentDetails += "<>Description = " + elements[10].ToString();
            }
            CR1_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> PS1_Methode(string[] elements)
        {
            PS1 obj = new PS1();

            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.ReferenceIdent = elements[1].ToString();
                segmentDetails += " Reference Ident= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.StateorProvCode = elements[3].ToString();
                segmentDetails += "<>State or Prov Code= " + elements[3].ToString();
            }
            PS1_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> SVD_Methode(string[] elements)
        {
            SVD obj = new SVD();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.IDCode = elements[1].ToString();
                segmentDetails += " ID Code = " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.CompMedProcedID = elements[3].ToString();
                segmentDetails += "<>Comp. Med Proced. ID= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.ProductServiceID = elements[4].ToString();
                segmentDetails += "<>Product /Service ID = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.Quantity = elements[5].ToString();
                segmentDetails += "<>Quantity = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.AssignedNumber = elements[6].ToString();
                segmentDetails += "<>Assigned Number = " + elements[6].ToString();
            }
            SVD_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CRC_Methode(string[] elements)
        {
            CRC obj = new CRC();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.CodeCategory = elements[1].ToString();
                segmentDetails += " Code Category= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.YesNoCondRespCode = elements[2].ToString();
                segmentDetails += "<>Yes/No Cond Resp Code= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.CertificateCondCode = elements[3].ToString();
                segmentDetails += "<>Certificate Cond Code= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.CertificateCondCodeR1 = elements[4].ToString();
                segmentDetails += "<>Certificate Cond Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.CertificateCondCodeR2 = elements[5].ToString();
                segmentDetails += "<>Certificate Cond Code = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.CertificateCondCodeR3 = elements[6].ToString();
                segmentDetails += "<>Certificate Cond Code = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.CertificateCondCodeR4 = elements[7].ToString();
                segmentDetails += "<>Certificate Cond Code = " + elements[7].ToString();
            }
            CRC_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CR2_Methode(string[] elements)
        {
            string segmentDetails = "";

            if (elements[1] != "")
            {
                segmentDetails += " Count = " + elements[1].ToString() + "(CCYYMMDD)";
            }
            if (elements[2] != "")
            {
                segmentDetails += "<>Quantity = " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                segmentDetails += "<>Subluxation Level Code = " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                segmentDetails += "<>Subluxation Level Code = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                segmentDetails += "<>Unit/Basis Meas Code = " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                segmentDetails += "<>Quantity = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                segmentDetails += "<>Quantity = " + elements[7].ToString();
            }

            if (elements[8] != "")
            {
                segmentDetails += "<>Nature of Cond Code = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                segmentDetails += "<>Yes/No Cond Resp Code = " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                segmentDetails += "<>Description = " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                segmentDetails += "<>Description = " + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                segmentDetails += "<>Yes/No Cond Resp Code = " + elements[12].ToString();
            }
            return segmentDetails;
        }
        public async Task<string> HCP_Methode(string[] elements)
        {
            HCP obj = new HCP();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.PricingMethodology = elements[1].ToString();
                segmentDetails += " Pricing Methodology= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.MonetaryAmountR1 = elements[3].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.ReferenceIdent = elements[4].ToString();
                segmentDetails += "<>Reference Ident= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.Rate = elements[5].ToString();
                segmentDetails += "<>Rate= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.ReferenceIdentR1 = elements[6].ToString();
                segmentDetails += "<>Reference Ident= " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.MonetaryAmountR2 = elements[7].ToString();
                segmentDetails += "<>Monetary Amount = " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.ProductServiceID = elements[8].ToString();
                segmentDetails += "<>Product Service ID = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.ProdServIDQual = elements[9].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.ProductServiceID = elements[10].ToString();
                segmentDetails += "<>Product/ Service ID = " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.UnitBasisMeasCode = elements[11].ToString();
                segmentDetails += "<>Unit/Basis Meas Code = " + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                obj.Quantity = elements[12].ToString();
                segmentDetails += "<>Quantity= " + elements[12].ToString();
            }
            if (elements[13] != "")
            {
                obj.RejectReasonCode = elements[13].ToString();
                segmentDetails += "<>Reject Reason Code= " + elements[13].ToString();
            }
            if (elements[14] != "")
            {
                obj.PolicyCompCode = elements[14].ToString();
                segmentDetails += "<>Policy Comp Code= " + elements[14].ToString();
            }
            if (elements[15] != "")
            {
                obj.ExceptionCode = elements[15].ToString();
                segmentDetails += "<>Exception Code= " + elements[15].ToString();
            }
            HCP_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> SV5_Methode(string[] elements)
        {
            SV5 obj = new SV5();
            string segmentDetails = "";
            if (elements[1] != "")
            {
                obj.CompMedProcedID = elements[1].ToString();
                segmentDetails += " Comp. Med Proced.ID= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.UnitBasiMeasCode = elements[2].ToString();
                segmentDetails += "<>Unit/Basi Meas Code= " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.Quantity = elements[3].ToString();
                segmentDetails += "<>Quantity= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.MonetaryAmount = elements[4].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.MonetaryAmountR1 = elements[5].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.FrequencyCode = elements[6].ToString();
                segmentDetails += "<>Frequency Code= " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.PrognosisCode = elements[7].ToString();
                segmentDetails += "<>Prognosis Code= " + elements[7].ToString();
            }
            SV5_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> CTP_Methode(string[] elements)
        {
            CTP obj = new CTP();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.ClassofTradeCode = elements[1].ToString();
                segmentDetails += " Class of Trade Code= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.PriceIDCode = elements[2].ToString();
                segmentDetails += "<>Price ID Code= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.UnitPrice = elements[3].ToString();
                segmentDetails += "<>Unit Price= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.Quantity = elements[4].ToString();
                segmentDetails += "<>Quantity= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.CompositeUnitofMea = elements[5].ToString();
                segmentDetails += "<>Composite Unit of Mea= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.PriceMultQualifier = elements[6].ToString();
                segmentDetails += "<>Price Mult Qualifier= " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.Multiplier = elements[7].ToString();
                segmentDetails += "<>Multiplier= " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.MonetaryAmount = elements[8].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.BasisUnitPriceCode = elements[9].ToString();
                segmentDetails += "<>Basis Unit Price Code= " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.ConditionValue = elements[10].ToString();
                segmentDetails += "<>Condition Value = " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.MultPriceQuantity = elements[11].ToString();
                segmentDetails += "<>Mult Price Quantity= " + elements[11].ToString();
            }
            CTP_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> SV1_Methode(string[] elements)
        {
            SV1 obj = new SV1();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.CompMedProcedID = elements[1].ToString();
                segmentDetails += "Comp. Med Proced.ID=" + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.MonetaryAmount = elements[2].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[2].ToString();
            }

            if (elements[3] != "")
            {
                obj.UnitBasiMeasCode = elements[3].ToString();
                segmentDetails += "<>Unit/Basi Meas Code= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.Quantity = elements[4].ToString();
                segmentDetails += "<>Quantity= " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.FacilityCode = elements[5].ToString();
                segmentDetails += "<>Facility Code= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.ServiceTypeCode = elements[6].ToString();
                segmentDetails += "<>Service Type Code= " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.CompDiagCodePoint = elements[7].ToString();
                segmentDetails += "<>Comp. Diag Code Point= " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.MonetaryAmountR1 = elements[8].ToString();
                segmentDetails += "<>Monetary Amount = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.YesNoCondRespCode = elements[9].ToString();
                segmentDetails += "<>Yes/No Cond Resp Code = " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.MultipleProcCode = elements[10].ToString();
                segmentDetails += "<>Multiple Proc Code= " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.YesNoCondRespCodeR1 = elements[11].ToString();
                segmentDetails += "<>Yes/No Cond Resp Code = " + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                obj.YesNoCondRespCodeR2 = elements[12].ToString();
                segmentDetails += "<>Yes/No Cond Resp Code= " + elements[12].ToString();
            }
            if (elements[13] != "")
            {
                obj.ReviewCode = elements[13].ToString();
                segmentDetails += "<>Review Code= " + elements[13].ToString();
            }
            if (elements[14] != "")
            {
                obj.NatlLocalRevValue = elements[14].ToString();
                segmentDetails += "<>Natl/Local Rev Value= " + elements[14].ToString();
            }
            if (elements[15] != "")
            {
                obj.CopayStatusCode = elements[15].ToString();
                segmentDetails += "<>Copay Status Code= " + elements[15].ToString();
            }
            if (elements[16] != "")
            {
                obj.HealthcareShortCode = elements[16].ToString();
                segmentDetails += "<>Healthcare Short Code= " + elements[16].ToString();
            }
            if (elements[17] != "")
            {
                obj.ReferenceIdent = elements[17].ToString();
                segmentDetails += "<>Reference Ident= " + elements[17].ToString();
            }
            if (elements[18] != "")
            {
                obj.PostalCode = elements[18].ToString();
                segmentDetails += "<>Postal Code= " + elements[18].ToString();
            }
            if (elements[19] != "")
            {
                obj.MonetaryAmountR2 = elements[19].ToString();
                segmentDetails += "<>Monetary Amount= " + elements[19].ToString();
            }
            if (elements[20] != "")
            {
                obj.LevelofCareCode = elements[20].ToString();
                segmentDetails += "<>Level of Care Code= " + elements[20].ToString();
            }
            if (elements[21] != "")
            {
                obj.ProviderAgreeCode = elements[21].ToString();
                segmentDetails += "<>Provider Agree Code= " + elements[21].ToString();
            }
            SV1_Segment.Add(obj);
            return segmentDetails;
        }
        public async Task<string> LIN_Methode(string[] elements)
        {
            LIN obj = new LIN();
            string segmentDetails = "";

            if (elements[1] != "")
            {
                obj.LINID = elements[1].ToString();
                segmentDetails += " LIN ID= " + elements[1].ToString();
            }
            if (elements[2] != "")
            {
                obj.ProdServIDQual = elements[2].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[2].ToString();
            }
            if (elements[3] != "")
            {
                obj.ProductServiceIDPrice = elements[3].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[3].ToString();
            }
            if (elements[4] != "")
            {
                obj.ProdServIDQual = elements[4].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[4].ToString();
            }
            if (elements[5] != "")
            {
                obj.ProductServiceIDPriceR1 = elements[5].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[5].ToString();
            }
            if (elements[6] != "")
            {
                obj.ProdServIDQualR1 = elements[6].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[6].ToString();
            }
            if (elements[7] != "")
            {
                obj.ProductServiceIDPriceR2 = elements[7].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[7].ToString();
            }
            if (elements[8] != "")
            {
                obj.ProdServIDQualR2 = elements[8].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[8].ToString();
            }
            if (elements[9] != "")
            {
                obj.ProductServiceIDPriceR3 = elements[9].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[9].ToString();
            }
            if (elements[10] != "")
            {
                obj.ProdServIDQualR3 = elements[10].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[10].ToString();
            }
            if (elements[11] != "")
            {
                obj.ProductServiceIDPriceR4 = elements[11].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[11].ToString();
            }
            if (elements[12] != "")
            {
                obj.ProdServIDQualR4 = elements[12].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[12].ToString();
            }
            if (elements[13] != "")
            {
                obj.ProductServiceIDPriceR5 = elements[13].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[13].ToString();
            }
            if (elements[14] != "")
            {
                obj.ProdServIDQualR5 = elements[14].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[14].ToString();
            }
            if (elements[15] != "")
            {
                obj.ProductServiceIDPriceR6 = elements[15].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[15].ToString();
            }
            if (elements[16] != "")
            {
                obj.ProdServIDQualR6 = elements[16].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[16].ToString();
            }
            if (elements[17] != "")
            {
                obj.ProductServiceIDPriceR7 = elements[17].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[17].ToString();
            }
            if (elements[18] != "")
            {
                obj.ProdServIDQualR7 = elements[18].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[18].ToString();
            }
            if (elements[19] != "")
            {
                obj.ProductServiceIDPriceR8 = elements[19].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[19].ToString();
            }
            if (elements[20] != "")
            {
                obj.ProdServIDQualR8 = elements[20].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[20].ToString();
            }
            if (elements[21] != "")
            {
                obj.ProductServiceIDPriceR9 = elements[21].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[21].ToString();
            }
            if (elements[22] != "")
            {
                obj.ProdServIDQualR9 = elements[22].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[22].ToString();
            }
            if (elements[23] != "")
            {
                obj.ProductServiceIDPriceR10 = elements[23].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[23].ToString();
            }
            if (elements[24] != "")
            {
                obj.ProdServIDQualR10 = elements[24].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[24].ToString();
            }
            if (elements[25] != "")
            {
                obj.ProductServiceIDPriceR11 = elements[25].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[25].ToString();
            }
            if (elements[26] != "")
            {
                obj.ProdServIDQualR11 = elements[26].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[26].ToString();
            }
            if (elements[27] != "")
            {
                obj.ProductServiceIDPriceR12 = elements[27].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[27].ToString();
            }
            if (elements[28] != "")
            {
                obj.ProdServIDQualR12 = elements[28].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[28].ToString();
            }
            if (elements[29] != "")
            {
                obj.ProductServiceIDPriceR13 = elements[29].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[29].ToString();
            }
            if (elements[30] != "")
            {
                obj.ProdServIDQualR14 = elements[30].ToString();
                segmentDetails += "<>Prod/Serv ID Qual = " + elements[30].ToString();
            }
            if (elements[31] != "")
            {
                obj.ProductServiceIDPriceR14 = elements[31].ToString();
                segmentDetails += "<>Product/Service ID Price= " + elements[31].ToString();
            }
            LIN_Segment.Add(obj);
            return segmentDetails;
        }
    }
}