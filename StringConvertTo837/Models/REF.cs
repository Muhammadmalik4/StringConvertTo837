﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class REF
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdentQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdentifier { get; set; }
    }
}
