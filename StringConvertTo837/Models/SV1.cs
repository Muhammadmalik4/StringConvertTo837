﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class SV1
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompMedProcedID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasiMeasCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string FacilityCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ServiceTypeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompDiagCodePoint { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MultipleProcCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCodeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCodeR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReviewCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string NatlLocalRevValue { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CopayStatusCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthcareShortCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PostalCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string LevelofCareCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProviderAgreeCode { get; set; }
    }
}
