﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class DTP
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeQualifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeFormatQua { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimePeriod { get; set; }

    }
}
