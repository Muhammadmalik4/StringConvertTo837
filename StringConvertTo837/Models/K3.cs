﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class K3
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string FixedFormInformation { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string RecordFormatCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompositeUnitofMea { get; set; }
    }
}
