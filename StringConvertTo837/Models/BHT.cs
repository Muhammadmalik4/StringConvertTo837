﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class BHT
        
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HierarchStructCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string TSPurposeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Date { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Time { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string TransactionTypeCode { get; set; }
    }
}
