﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CTP
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClassofTradeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PriceIDCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitPrice { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompositeUnitofMea { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PriceMultQualifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Multiplier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string BasisUnitPriceCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ConditionValue{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MultPriceQuantity { get; set; }
    }
}
