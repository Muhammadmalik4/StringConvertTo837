﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class PAT
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IndividualRelatCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PatientLocCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string EmploymentStatuSCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string StudentStatusCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeFormatQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimePeriod { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasisMeasCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Weight { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCode { get; set; }
    }
}
