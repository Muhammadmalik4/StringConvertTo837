﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class N3
    {

        [Required(ErrorMessage = "Mandatory Field.")]
        public string AddressInformation { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AddressInformationR1 { get; set; }
    }
}
