﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class SVD
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IDCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompMedProcedID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AssignedNumber { get; set; }
    }
}
