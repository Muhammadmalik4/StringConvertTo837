﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CUR
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string EntityIDCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CurrencyCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ExchangeRate { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string EntityIDCodeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CurrencyCodeR1{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CurrMarketExchgCode{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeQualifier{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime Time { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeQualifierR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime DateR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime TimeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeQualifierR2{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime DateR2{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime TimeR2{ get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeQualifierR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime DateR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime TimeR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeQualifierR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime DateR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public DateTime TimeR4 { get; set; }
    }
}
