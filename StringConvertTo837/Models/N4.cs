﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class N4
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CityName { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string StateorProvCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PostalCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CountryCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string LocationQualifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string LocationIdentifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CountrySubCode { get; set; }
    }
}
