﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class DMG
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimeFormatQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DateTimePeriod { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string GenderCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MaritalStatusCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompRaceorEthnInf { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CitizenshipStatusCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CountryCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string BasisofVerifCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CodeListQualCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IndustryCode { get; set; }
    }
}
