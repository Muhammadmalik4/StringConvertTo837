﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class PWK
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReportTypeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReportTransmCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReportCopiesNeed { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string EntityIDCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IDCodeQualifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IDCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ActionsIndicated { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string RequestCategCode { get; set; }
    }
}
