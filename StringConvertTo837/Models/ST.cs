﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class ST
    {
        [Required(ErrorMessage ="Required Field.")]
        [Range(3,3 ,ErrorMessage ="Accept only 3 digits value.")]

        public double IdCode { get; set; }

        [Required(ErrorMessage = "Required Field.")]
        [Range(4, 9, ErrorMessage = "Accept only 4-9 digits value.")]
        public string ControlNumber { get; set; }

        [Required(ErrorMessage = "Required Field.")]
        [Range(1, 35, ErrorMessage = "Accept only 1-35 digits value.")]
        public string ImpleConvReference { get; set; }
    }
}
