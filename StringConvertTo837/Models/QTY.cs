﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class QTY
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string QuantityQualifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompositeUnitofMea { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string FreeFormMessage { get; set; }
    }
}
