﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class HL
    {

        [Required(ErrorMessage = "Mandatory Field.")]
        public string HierarchIDNumber { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HierarchParentID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HierarchLevelCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HierarchChildCode { get; set; }
    }
}
