﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class HI
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfo { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR5 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR6 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR7 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR8 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR9 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR10 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareCodeInfoR11 { get; set; }
    }
}
