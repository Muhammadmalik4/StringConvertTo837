﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class SBR
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PayerRespSeqNoCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IndividualRelatCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string InsuranceTypeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string BenefitsCoordCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string EmploymentStatusCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimFileIndCode { get; set; }
    }
}
