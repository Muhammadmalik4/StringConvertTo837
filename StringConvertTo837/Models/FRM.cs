﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class FRM
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AssignedID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Date { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AllowChrgPercent { get; set; }
    }
}
