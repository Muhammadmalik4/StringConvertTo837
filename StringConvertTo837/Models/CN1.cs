﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CN1
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ContractTypeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AllowChrgPercent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string TermsDiscPercent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string VersionID{ get; set; }
    }
}
