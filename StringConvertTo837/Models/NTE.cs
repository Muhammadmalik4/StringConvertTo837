﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class NTE
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string NoteRefCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Description { get; set; }
    }
}
