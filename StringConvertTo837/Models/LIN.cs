﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class LIN
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string LINID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPrice { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR5 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR5 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR6 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR6 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR7 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR7 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR8 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR8 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR9 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR9 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR10 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR10 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR11 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR11 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR12 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR12 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR13 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR13 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQualR14 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDPriceR14 { get; set; }
    }
}
