﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class LQ
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CodeListQualCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string IndustryCode { get; set; }
    }
}
