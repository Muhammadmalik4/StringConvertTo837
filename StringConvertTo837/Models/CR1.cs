﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CR1
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasisMeasCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Weight { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AmbulanceTransCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AmbulanceReasonCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasisMeasCodeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AddressInformation { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AddressInformationR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DescriptionR1 { get; set; }
    }
}
