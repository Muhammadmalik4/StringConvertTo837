﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CAS

    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjGroupCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjReasonCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjReasonCodeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string QuantityR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjReasonCodeR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string QuantityR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjReasonCodeR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string QuantityR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjReasonCodeR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string QuantityR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimAdjReasonCodeR5 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR5 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string QuantityR5 { get; set; }
    }
}
