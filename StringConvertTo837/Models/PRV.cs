﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class PRV
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProviderCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdentQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string StateorProvCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProviderSpecInf { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProviderOrgCode { get; set; }
    }
}
