﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CR3
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CertificateTypeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasisMeasCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string InsulinDependCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Description { get; set; }
    }
}
