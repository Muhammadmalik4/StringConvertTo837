﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class PER
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ContactFunctCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CommNumberQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CommNumber { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CommNumberQualR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CommNumberR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CommNumberQualR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CommNumberR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ContactInqReference { get; set; }
    }
}
