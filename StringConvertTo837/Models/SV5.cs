﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class SV5
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CompMedProcedID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasiMeasCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string FrequencyCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PrognosisCode { get; set; }
    }
}
