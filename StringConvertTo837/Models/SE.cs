﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class SE
    {

        [Required(ErrorMessage = "Mandatory Field.")]
        public string NumberofIncSegs { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ControlNumber { get; set; }
    }
}
