﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CLM
    {

        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimSubmtIdentifier { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimFileIndCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string NonInstClaimCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string HealthCareServLoc { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProviderAcceptCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCodeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReleaseofInfoCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PatientSigSourceCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string RelatedCausesInfo { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string SpecialProgCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCodeR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string LevelofServCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCodeR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProviderAgreeCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimStatusCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCodeR4 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ClaimSubmtReasonCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string DelayReasonCode { get; set; }
    }
}
