﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class CRC
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CodeCategory { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string YesNoCondRespCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CertificateCondCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CertificateCondCodeR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CertificateCondCodeR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CertificateCondCodeR3 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string CertificateCondCodeR4 { get; set; }
    }
}
