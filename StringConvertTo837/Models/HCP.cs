﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class HCP
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PricingMethodology { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmount { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdent { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Rate { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ReferenceIdentR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string MonetaryAmountR2 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceID { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProdServIDQual { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ProductServiceIDR1 { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string UnitBasisMeasCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string Quantity { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string RejectReasonCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string PolicyCompCode { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string ExceptionCode { get; set; }
    }
}
