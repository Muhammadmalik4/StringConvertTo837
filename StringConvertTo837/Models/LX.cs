﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class LX
    {
        [Required(ErrorMessage = "Mandatory Field.")]
        public string AssignedNumber{ get; set; }
    }
}
