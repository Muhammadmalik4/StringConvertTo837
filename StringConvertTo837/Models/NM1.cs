﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StringConvertTo837.Models
{
    public class NM1
    {
       [Required(ErrorMessage = "Mandatory Field.")]
         public string EntityIDCode { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string EntityTypeQualifier { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string NameLastOrgName { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string NameFirst { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string NameMiddle { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string NamePrefix{ get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string NameSuffix{ get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string IDCodeQualifier { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string IDCode { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string EntityRelatCode { get; set; }
       [Required(ErrorMessage = "Mandatory Field.")]
         public string EntityIDCodeReapet { get; set; }
        [Required(ErrorMessage = "Mandatory Field.")]
        public string NameLastOrgNameReapet{ get; set; }
    }
}
